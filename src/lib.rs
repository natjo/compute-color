extern crate image;
extern crate palette;
extern crate arraymap;
extern crate hex;

use std::collections::HashMap;
use std::fs;
use std::error::Error;
use std::process;


pub fn run(img_dir: &str, output_path: &str, resize: bool, img_dim: (u32, u32), min_max_hue: (f32, f32), min_brightness: f32, min_saturation: f32, step_size: u8) -> Result<(), Box<dyn Error>> {
    let paths = get_paths(img_dir);

    let mut pixel_map: HashMap<image::Rgb<u8>, i32> = HashMap::new();
    let mut cnt = 1;
    let num_images = paths.len();
    for path in paths {
        println!("Analyze image \"{}\" [{}/{}]", path.display(), cnt, num_images);
        match load_img(&path, resize, img_dim) {
            Ok(img) => update_pixel_map(&mut pixel_map, &img, min_max_hue, min_brightness, min_saturation, step_size),
            Err(_e) => println!("Can't open image \"{}\"", path.display()),
        };
        cnt += 1;
    }

    println!("Calculate mode color");
    // println!("{:?}", pixel_map);
    let max_color = get_max_color(&pixel_map);
    println!("Mode color: rgb:{:?}", max_color.data);

    save_color(&max_color.data, output_path);

    Ok(())
}

fn get_paths(img_dir: &str) -> std::vec::Vec<std::path::PathBuf>{
    let entries = match fs::read_dir(img_dir){
        Ok(entries) => entries,
        Err(_e) => {
            println!("directory \"{}\" not found", img_dir);
            process::exit(1);
        },
    };

    let paths: std::vec::Vec<_> = entries.map(|res| res.unwrap().path()).collect();
    paths
}

fn load_img(img_path: &std::path::PathBuf, resize: bool, img_dim: (u32, u32))
    -> Result<image::ImageBuffer<image::Rgb<u8>, std::vec::Vec<u8>>, image::ImageError> {
        let mut img = image::open(img_path)?;
        if resize {
            img = img.resize_exact(img_dim.0, img_dim.1 , image::FilterType::Gaussian);
        }
        let img = img.to_rgb();
        // let dim = img.dimensions();
        Ok(img)
    }

pub fn save_color(data: &[u8], path: &str){
    let data = format!("#{}", hex::encode(data));
    fs::write(path, data).expect(&format!("Unable to write to file \"{}\"", path));
}

fn update_pixel_map(pixel_map: &mut HashMap<image::Rgb<u8>, i32>, img: &image::RgbImage, min_max_hue: (f32, f32), min_brightness: f32, min_saturation: f32, step_size: u8){
    for pixel in img.pixels() {
        let simple_pixel = simplify_pixel(pixel, step_size);
        if pixel_allowed(&simple_pixel, min_max_hue, min_brightness, min_saturation){
            let count = pixel_map.entry(simple_pixel.clone()).or_insert(0);
            *count += 1;
        }
    }
}

fn simplify_pixel(pixel: &image::Rgb<u8>, step_size: u8) -> image::Rgb<u8> {
    let color_pixel_simplified = pixel.data.map(|x| (x as f32 / step_size as f32).round() as u8 * step_size);
    image::Rgb(color_pixel_simplified)
}

// this function checks if a pixel is inside a given color range
// min_max_hue describe minimal and maximal value: (min, max)
// if min > max, check if hue is outside these
fn pixel_allowed(pixel: &image::Rgb<u8>, min_max_hue: (f32, f32), min_brightness: f32, min_saturation: f32) -> bool {
    let color_pixel = pixel.data.map(|x| (x as f32)/255.);
    let color_hsv = palette::Hsv::from(palette::LinSrgb::new(color_pixel[0], color_pixel[1], color_pixel[2]));
    // println!("{:?} to {:?}", pixel.data, color_hsv);

    if color_hsv.saturation < min_saturation || color_hsv.value < min_brightness{
        return false;
    }else if min_max_hue.0 < min_max_hue.1{
        // check if hue is inside
        color_hsv.hue.to_positive_degrees() >= min_max_hue.0 && color_hsv.hue.to_positive_degrees() <= min_max_hue.1
    }else{
        // check if hue is outside
        color_hsv.hue.to_positive_degrees() >= min_max_hue.0 || color_hsv.hue.to_positive_degrees() <= min_max_hue.1
    }
}

fn get_max_color<'a>(pixel_map: &'a HashMap<image::Rgb<u8>, i32>) -> &'a image::Rgb<u8>{
    pixel_map
        .iter()
        .max_by_key(|&(_, count)| count)
        .map(|(val, _)| val)
        .expect("Cannot compute the mode of zero numbers")
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn load_paths() {
        let test_dir = "img_test";
        let exp_paths = vec![
            std::path::PathBuf::from("img_test/black.jpg"),
            std::path::PathBuf::from("img_test/black_white.jpg"),
            std::path::PathBuf::from("img_test/white.jpg"),
            std::path::PathBuf::from("img_test/pink.png"),
            std::path::PathBuf::from("img_test/black_white_red.png"),
            std::path::PathBuf::from("img_test/cat.jpg")
        ];
        let paths = get_paths(test_dir);
        assert_eq!(exp_paths, paths);
    }

    #[test]
    fn load_img_black() {
        let test_img_path = std::path::PathBuf::from("img_test/black.jpg");
        let exp_img_dims = (188, 268);
        let exp_img_color = [0,0,0];
        let img = load_img(&test_img_path, false, (100,100)).unwrap();
        assert_eq!(exp_img_dims, img.dimensions());
        assert_eq!(exp_img_color, img.get_pixel(0,0).data);
    }

    #[test]
    fn load_img_white() {
        let test_img_path = std::path::PathBuf::from("img_test/white.jpg");
        let exp_img_dims = (550,435);
        let exp_img_color = [255,255,255];
        let img = load_img(&test_img_path, false, (100,100)).unwrap();
        assert_eq!(exp_img_dims, img.dimensions());
        assert_eq!(exp_img_color, img.get_pixel(0,0).data);
    }

    #[test]
    fn test_pixel_map() {
        let test_img_path = std::path::PathBuf::from("img_test/black.jpg");
        let exp_img_color = image::Rgb{data:[0u8,0u8,0u8]};
        let exp_color_count = 100*100;

        let img = load_img(&test_img_path, true, (100,100)).unwrap();
        let mut pixel_map:HashMap<image::Rgb<u8>, i32> = HashMap::new();
        update_pixel_map(&mut pixel_map, &img, (0., 0.), 0., 0., 3);
        assert_eq!(&exp_color_count, pixel_map.get(&exp_img_color).unwrap());
    }

    #[test]
    fn test_find_mode_single_image() {
        let test_img_path = std::path::PathBuf::from("img_test/black_white.jpg");
        let exp_max_color = image::Rgb{data:[0u8,0u8,0u8]};

        let img = load_img(&test_img_path, true, (100,100)).unwrap();
        let mut pixel_map: HashMap<image::Rgb<u8>, i32> = HashMap::new();
        update_pixel_map(&mut pixel_map, &img, (0., 0.), 0., 0., 3);
        let max_color = get_max_color(&pixel_map);
        assert_eq!(&exp_max_color, max_color);
    }

    #[test]
    fn test_find_mode_single_image_red() {
        let test_img_path = std::path::PathBuf::from("img_test/black_white_red.png");
        let exp_max_color = image::Rgb{data:[255u8,0u8,0u8]};

        let img = load_img(&test_img_path, false, (100,100)).unwrap();
        let mut pixel_map: HashMap<image::Rgb<u8>, i32> = HashMap::new();
        update_pixel_map(&mut pixel_map, &img, (0., 20.), 0.3, 0.3, 3);
        let max_color = get_max_color(&pixel_map);
        println!("{:?}", pixel_map);
        assert_eq!(&exp_max_color, max_color);
    }

    #[test]
    fn test_find_mode_single_image_red_inverted() {
        let test_img_path = std::path::PathBuf::from("img_test/black_white_red.png");
        let exp_max_color = image::Rgb{data:[255u8,0u8,0u8]};

        let img = load_img(&test_img_path, false, (100,100)).unwrap();
        let mut pixel_map: HashMap<image::Rgb<u8>, i32> = HashMap::new();
        update_pixel_map(&mut pixel_map, &img, (300., 20.), 0.3, 0.3, 3);
        let max_color = get_max_color(&pixel_map);
        println!("{:?}", pixel_map);
        assert_eq!(&exp_max_color, max_color);
    }

    #[test]
    fn test_find_mode_single_image_pink() {
        let test_img_path = std::path::PathBuf::from("img_test/pink.png");
        let exp_max_color = image::Rgb{data:[255u8,0u8,255u8]};

        let img = load_img(&test_img_path, false, (100,100)).unwrap();
        let mut pixel_map: HashMap<image::Rgb<u8>, i32> = HashMap::new();
        update_pixel_map(&mut pixel_map, &img, (280., 40.), 0.3, 0.3, 3);
        let max_color = get_max_color(&pixel_map);
        println!("{:?}", pixel_map);
        assert_eq!(&exp_max_color, max_color);
    }

    #[test]
    fn test_find_mode_multiple_images() {
        let paths = get_paths("img_test");

        let mut pixel_map: HashMap<image::Rgb<u8>, i32> = HashMap::new();
        for path in paths {
            let img = load_img(&path, true, (100,100)).unwrap();
            update_pixel_map(&mut pixel_map, &img, (0., 0.), 0., 0., 3);
        }

        let max_color = get_max_color(&pixel_map);
        let exp_max_color = image::Rgb{data:[0u8,0u8,0u8]};

        assert_eq!(&exp_max_color, max_color);
    }

    #[test]
    fn test_verify_color_simplification() {
        let step_size = 3;
        let input_color1 = image::Rgb{data:[0u8,0u8,0u8]};
        let exp_color1 = image::Rgb{data:[0u8,0u8,0u8]};
        assert_eq!(exp_color1, simplify_pixel(&input_color1, step_size));

        let input_color2 = image::Rgb{data:[2u8,0u8,1u8]};
        let exp_color2 = image::Rgb{data:[3u8,0u8,0u8]};
        assert_eq!(exp_color2, simplify_pixel(&input_color2, step_size));

        let input_color3 = image::Rgb{data:[255u8,200u8,19u8]};
        let exp_color3 = image::Rgb{data:[255u8,201u8,18u8]};
        assert_eq!(exp_color3, simplify_pixel(&input_color3, step_size));
    }
}
