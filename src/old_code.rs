fn create_hsv_color_distribution_img(pixel_map: &HashMap<image::Rgb<u8>, i32>){
    let mut imgbuf = image::ImageBuffer::new(200,100);
    for (x, y, pixel) in imgbuf.enumerate_pixels_mut() {
        let h = (x as f32)/200.;
        let s = (y as f32)/100.;
        let v = (y as f32)/200.;
        let color = palette::Srgb::from(palette::Hsv::new(h, s, v));
        let r = (color.red * 255.) as u8;
        let g = (color.green * 255.) as u8;
        let b = (color.blue * 255.) as u8;
        let a = 255 as u8;
        *pixel = image::Rgba([r,g,b,a]);
        println!("{:?}", color);
        println!("{:?}", image::Rgba([r,g,b,a]));

    }
    imgbuf.save("color_distribution.png").unwrap();
}

fn create_rgb_color_distribution_img(pixel_map: &HashMap<image::Rgb<u8>, i32>){
    let max_val = *pixel_map.get(&get_max_color(&pixel_map)).unwrap() as f32;

    let mut imgbuf = image::ImageBuffer::new(4096,4096);
    // Iterate over the coordinates and pixels of the image
    let mut cnt: f32 = 0.0;
    let max: f32 = 256.0;
    for (_x, _y, pixel) in imgbuf.enumerate_pixels_mut() {
        let mut r = cnt % max;
        let g = ((cnt/max).floor() % max) as u8;
        let b = ((cnt/(max*max)).floor() % max) as u8;

        // Flip every second red gradient to make the image smoother
        if g % 2 == 1{
            r = 255.0 - r;
        }

        // Calculate the transparency
        let color = image::Rgb{data:[r as u8, g, b]};
        let num_occurences = match pixel_map.get(&color){
            Some(arg) => *arg as f32,
            None => 0.0,
        };

        let a = ((num_occurences / max_val).ceil() * 255.0) as u8;

        *pixel = image::Rgba([r as u8, g, b, a]);
        cnt += 1.0;
    }
    imgbuf.save("color_distribution.png").unwrap();
}
