pub fn gen_tuple_from_vec<T: Clone>(input: std::vec::Vec<T>) -> (T, T){
    (input[0].clone(), input[1].clone())
}


