mod lib;
mod utils;

use std::process;

extern crate clap;
use clap::{Arg, App};

static IMAGE_DIMENSIONS: (u32, u32) = (100, 100);
static MIN_MAX_HUE: (f32, f32) = (0., 360.);
static MIN_SATURATION: &str = "0.3";
static MIN_BRIGHTNESS: &str = "0.3";
static STEP_SIZE: &str = "3";

fn main() {
    let matches = App::new("Most Used Color Finder")
        .about("Calculates the most present color in a set of images")
        .arg(Arg::with_name("image-directory")
             .short("d")
             .long("image-directory")
             .value_name("IMAGE_DIRECTORY")
             .help("directory which contains all images to analyze")
             .required(true))
        .arg(Arg::with_name("output-file")
             .short("o")
             .long("output-file")
             .value_name("OUTPUT_FILE")
             .help("file to write the final hex code to")
             .required(true))
        .arg(Arg::with_name("resize-dimensions")
             .short("r")
             .long("resize-dimensions")
             .value_name("X_Y")
             .help("expects 2 arguments, specifieng the x and y dimensions all images are transformed to before analysis")
             .multiple(true))
        .arg(Arg::with_name("no-resize")
             .long("no-resize")
             .help("if set, algorithm will not resize images")
             .takes_value(false))
        .arg(Arg::with_name("min-max-hue")
             .long("hue")
             .value_name("MIN_MAX_HUE")
             .help("expects 2 arguments, specifying the minimal and maximum hue, must be between 0 and 360. To check outside this range, name the max value first")
             .multiple(true))
        .arg(Arg::with_name("min-saturation")
             .short("s")
             .long("min-saturation")
             .value_name("MIN_SATURATION")
             .help("the minimal value for saturation, must be between 0 and 1")
             .default_value(MIN_SATURATION))
        .arg(Arg::with_name("min-brightness")
             .short("b")
             .long("min-brightness")
             .value_name("MIN_BRIGHTNESS")
             .help("the minimal value for brightness, must be between 0 and 1")
             .default_value(MIN_BRIGHTNESS))
        .arg(Arg::with_name("step-size")
             .long("step-size")
             .value_name("STEP-SIZE")
             .help("color values are rounded in these steps")
             .default_value(STEP_SIZE))
        .get_matches();

    let img_dir = matches.value_of("image-directory").unwrap();
    let output_file = matches.value_of("output-file").unwrap();
    let resize = !matches.is_present("no-resize");
    let dimensions: (u32, u32) = match matches.values_of("resize-dimensions"){
        Some(arg) => utils::gen_tuple_from_vec(arg.map(|val| val.parse::<u32>().unwrap()).collect()),
        None      => IMAGE_DIMENSIONS
    };
    let min_max_hue: (f32, f32) = match matches.values_of("min-max-hue"){
        Some(arg) => utils::gen_tuple_from_vec(arg.map(|val| val.parse::<f32>().unwrap()).collect()),
        None      => MIN_MAX_HUE
    };
    let min_saturation: f32 = matches.value_of("min-saturation").unwrap().parse::<f32>().unwrap();
    let min_brightness: f32 = matches.value_of("min-brightness").unwrap().parse::<f32>().unwrap();

    let step_size: u8 = matches.value_of("step-size").unwrap().parse::<u8>().unwrap();

    println!("Analyze images in directory \"{}\"", img_dir);

    if let Err(e) = lib::run(img_dir, output_file, resize, dimensions, min_max_hue, min_brightness, min_saturation, step_size) {
        println!("Application error: {}", e);
        process::exit(1);
    }
}

