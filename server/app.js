var bodyParser = require('body-parser');
var express = require('express');
var app = express();

var renderFun = require('./src/renderFun.js');

app.use(bodyParser.urlencoded({
    extend: true
}));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.set('views', __dirname);

app.get('/', function(req, res) {
    renderFun.handleIndexPage(res);
});

app.get('/penispink', function(req, res) {
    renderFun.handleColorPage(res, "penis_pink");
});

app.get('/dottergelb', function(req, res) {
    renderFun.handleColorPage(res, "dotter_gelb");
});

app.get('/kastanienbraun', function(req, res) {
    renderFun.handleColorPage(res, "kastanienbraun");
});

app.use(express.static(__dirname + '/public'));

console.log('Listening on 8888');
app.listen(8888);
