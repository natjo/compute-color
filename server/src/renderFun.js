var fs = require('fs')

exports.handleColorPage = function(res, color) {
    fs.readFile(`public/colors/${color}.color`, function(err, buf) {
        if (err) {
            console.log(err)
            renderColorNotFoundPage(res);
        } else {
            renderColorPage(res, buf.toString());
        }
    });
}

exports.handleIndexPage = function(res) {
    getAllColors(function(colors) {
        fs.readFile(`public/colors/lastUpdate.txt`, function(err, buf) {
            var updateDate = {};
            if (err) {
                console.log(err)
                updateDate.updateDate = "No information available";
            } else {
                updateDate.updateDate = buf.toString();
            }
            res.render('public/index.html', { ...updateDate,
                ...colors
            });
        });
    });
}

function getAllColors(fun) {
    var colors = {};
    fs.readFile(`public/colors/dotter_gelb.color`, function(err, buf) {
        if (err) {
            console.log("Can't open dotter_gelb.color")
        }
        colors.colorDottergelb = buf.toString();
        fs.readFile(`public/colors/kastanienbraun.color`, function(err, buf) {
            if (err) {
                console.log("Can't open kastanienbraun.color")
            }
            colors.colorKastanienbraun = buf.toString();
            fs.readFile(`public/colors/penis_pink.color`, function(err, buf) {
                if (err) {
                    console.log("Can't open penis_pink.color")
                }
                colors.colorPenispink = buf.toString();
                fun(colors);
            })
        })
    });

}

function renderColorPage(res, color) {
    res.render('public/colorView.html', {
        name: color
    });
}

function renderColorNotFoundPage(res) {
    res.render('public/colorNotFound.html');
}
