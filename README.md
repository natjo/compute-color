# Color Finder
This project downloads a variable number of images for a google search and finds the most often occuring color throughout all images. It displays these through a simple node.js server. Run `install.sh` to set everything up. The binary created by cargo contains all documentation to use this project.

`run.sh` is currently not working completely. You can download images via it, but to analyze them look into `./analyze_colors/main.py` and adjust the constants for your usage.

## Usage
# 1.
`run.sh` eases the usage, as the download of images is not handled through the rust project, which only analyses the images. E.g.: `./run.sh "dotter gelb" 0 60` The last two numbers specify the range in which the computed hue must be.

# 2.
Add the cronjob as seen in the example and start the node server once.
