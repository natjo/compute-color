import cv2
import os
import numpy as np
from skimage import io
from sklearn.cluster import KMeans
import colorsys
import matplotlib.pyplot as plt

COLOR_PER_IMAGE = 5
FINAL_COLORS = 5
ITERATIONS_K_MEANS=50
DIRECTORY='./downloaded_images/jalapeno_gruen'


def main():
    palettes = None
    # Collect color information
    for file in os.listdir(DIRECTORY):
        image_path = f'{DIRECTORY}/{file}'
        print(f'Collect color info of {image_path}')
        palette = get_main_colors_image(image_path)
        if palettes is None:
            palettes = palette
        else:
            palettes = np.concatenate((palettes, palette))

    # Get main colors
    kmeans = KMeans(n_clusters=FINAL_COLORS, random_state=0).fit(palettes)
    main_colors = kmeans.cluster_centers_

    # Show final palette, let user decide
    show_palette(main_colors)


def show_palette(palette):
    colors_to_show = []
    for color in palette:
        print(f'RGB: {color}')
        colors_to_show.append([[x/256 for x in color]])
    print(colors_to_show)
    plt.imshow(colors_to_show)
    plt.show()


def get_main_colors_image(image_path):
    # Load image
    img = io.imread('./downloaded_images/jalapeno_gruen/1.jalapeno-grn_01.jpg')
    pixels = np.float32(img.reshape(-1, 3))

    # Run k-means clustering
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, ITERATIONS_K_MEANS, .1)
    flags = cv2.KMEANS_RANDOM_CENTERS
    _, labels, palette = cv2.kmeans(pixels, COLOR_PER_IMAGE, None, criteria, 10, flags)
    return palette


if __name__ == '__main__':
    main()
