#!/bin/bash

set -e

#install node modules
# npm install --prefix server

# Create virtual environment and install modules
python3 -m venv venv
./venv/bin/pip install requirements.txt

# Create the rust binary to analyze images
# cargo build --release
