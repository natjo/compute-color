#!/bin/bash

set -e

if [ $# -eq 0 ]
  then
    echo "No search string specified\nUsage: ./run.sh SEARCH_STRING MIN_HUE MAX_HUE"
fi

SEARCH_STRING="$1"
MIN_HUE=$2
MAX_HUE=$3
NUM_IMAGES=30  # Max is 100

# Compute all used file and directory names
currentDir="$( cd "$( dirname "$0" )" && pwd )"
searchStringNoWhitespace=$(sed 's/ /_/g' <<< "$SEARCH_STRING")
imageDownloadDirectory="${currentDir}/downloaded_images/${searchStringNoWhitespace}"

# colorOutputFile="${currentDir}/server/public/colors/${searchStringNoWhitespace}.color"
colorOutputFile="${currentDir}/colors/${searchStringNoWhitespace}.color"

# Create directory for images
rm -fr $imageDownloadDirectory # Clean in case there was data from an earlier run
mkdir -p $imageDownloadDirectory
mkdir -p ${currentDir}/colors

# Download images
# To find documentation: https://google-images-download.readthedocs.io/en/latest/arguments.html
${currentDir}/venv/bin/googleimagesdownload -k "${SEARCH_STRING}" -l ${NUM_IMAGES} -o ${imageDownloadDirectory} -n

# Analyze images
# ${currentDir}/target/release/get_max_color -d $imageDownloadDirectory --hue $MIN_HUE $MAX_HUE -o $colorOutputFile
